const roomList = [
  //   {
  //     roomName: "Room A",
  //     roomMaker: "Player 1",
  //     player1Choice: "rock",
  //     player2Choice: "rock",
  //     result: "draw",
  //   },
];

class GameModel {
  getAllGame = () => {
    return roomList;
  };

  isRoomExisted = (dataRegistered) => {
    const existRoom = roomList.find((data) => {
      return data.roomName === dataRegistered.roomName;
    });
    if (existRoom) {
      return true;
    } else {
      return false;
    }
  };

  creatingRoom = (dataRegistered) => {
    roomList.push({
      id: roomList.length + 1,
      roomName: dataRegistered.roomName,
    });
  };

  //   stopCreateRoom = ({ id }) => {
  //     const expectedTotalRoom = roomList.find((data) => {
  //       return id;
  //     });
  //   };
}

module.exports = new GameModel();
