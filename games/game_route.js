const express = require("express");
const gameRouter = express.Router();
const gameController = require("./game_controller");

gameRouter.get("/game", gameController.getAllGames);

gameRouter.post("/room", gameController.createARoom);

// userRouter.post("/login", userController.loginForUser);

module.exports = gameRouter;
