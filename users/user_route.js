const express = require("express");
const userRouter = express.Router();
const userController = require("./user_controller");
const authMiddleware = require("../middleware/auth_middleware");

userRouter.get("/user", authMiddleware, userController.getAllUsers);

userRouter.get("/detail/:id", authMiddleware, userController.getSingleUser);

userRouter.post("/registration", userController.registerUser);

userRouter.post("/login", userController.loginForUser);

userRouter.put("/detail/:id", authMiddleware, userController.updateUserBio);

userRouter.post("/gamehis", authMiddleware, userController.createGameHistory);

userRouter.get(
  "/gamehis/:id",
  authMiddleware,
  userController.getUserGameHistories
);

module.exports = userRouter;
